import 'package:flutter/material.dart';

Color yellowcolor = const Color(0xffFFE1AA);
Color accentcolorblue = const Color(0xff00C9FF);
Color accentcolor = const Color(0xff6A67FF);
Color accentcolordark = const Color(0xffF06449);
Color secondarycolor = const Color(0xff393A3D);
Color blackcolor = const Color(0xff242A37);
Color greycolor = const Color(0xffF0F0F0);
Color primarycolordark = const Color(0xff2C2D2F);

class Apptheme {
  Apptheme._();

  static final ThemeData lighttheme = ThemeData(
    accentColor: accentcolor,
    primaryColor: Colors.white,
    scaffoldBackgroundColor: greycolor,
    appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        textTheme: TextTheme(headline1: TextStyle(color: Colors.black))),
    iconTheme: IconThemeData(color: Colors.black),
    primaryIconTheme: IconThemeData(color: Colors.black),
    textTheme: TextTheme(
        subtitle1: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w500, fontSize: 20),
        subtitle2: TextStyle(color: Colors.black),
        headline1: TextStyle(color: Colors.black),
        headline2: TextStyle(color: Colors.black),
        headline3: TextStyle(color: Colors.black),
        headline4: TextStyle(color: Colors.black),
        headline5: TextStyle(color: Colors.black),
        caption: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w300, fontSize: 20)),
    primaryTextTheme: TextTheme(
        headline3: TextStyle(color: Colors.black, fontSize: 13),
        subtitle1: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
        subtitle2: TextStyle(color: Color(0xff00C9FF)),
        headline2: TextStyle(color: Colors.black),
        headline4: TextStyle(color: Colors.black, fontSize: 12),
        headline5: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
        caption: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w300, fontSize: 15)),
  );

  static final ThemeData darktheme = ThemeData(
    accentColor: accentcolordark,
    primaryColor: primarycolordark,
    scaffoldBackgroundColor: primarycolordark,
    appBarTheme: AppBarTheme(
        color: secondarycolor,
        iconTheme: IconThemeData(color: Colors.white),
        textTheme: TextTheme(headline1: TextStyle(color: Colors.white))),
    iconTheme: IconThemeData(color: Colors.white),
    primaryIconTheme: IconThemeData(color: Colors.white),
    textTheme: TextTheme(
        subtitle1: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20),
        subtitle2: TextStyle(color: Colors.white),
        headline1: TextStyle(color: Colors.white),
        headline2: TextStyle(color: Colors.white),
        headline3: TextStyle(color: Colors.white),
        headline4: TextStyle(color: Colors.white),
        headline5: TextStyle(color: Colors.white),
        caption: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w300, fontSize: 20)),
    primaryTextTheme: TextTheme(
        headline3: TextStyle(color: Colors.white, fontSize: 13),
        subtitle1: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16),
        subtitle2: TextStyle(color: Color(0xff00C9FF)),
        headline2: TextStyle(color: Colors.white),
        headline4: TextStyle(color: Colors.white, fontSize: 12),
        headline5: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16),
        caption: TextStyle(
            color: Colors.white, fontWeight: FontWeight.w300, fontSize: 15)),
  );
}
