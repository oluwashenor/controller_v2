import 'package:controller_v2/global.dart';
import 'package:controller_v2/main.dart';
import 'package:controller_v2/utils/sharedpref.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Gender extends StatefulWidget {
  @override
  _GenderState createState() => _GenderState();
}

class _GenderState extends State<Gender> {
  var gender;

  bool _tapped = true;

  bool _tappedfemale = false;

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: Container(
              padding: EdgeInsets.only(top: screenHeight * 0.05),
              color: Theme.of(context).primaryColor,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        child: Text(
                          "What's your gender ?",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 22,
                              fontWeight: FontWeight.bold),
                        )),
                    SizedBox(height: 20),
                    Expanded(
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              print("Male");
                              setState(() {
                                gender = "male";
                                _tapped = !_tapped;
                                if (_tapped = true) {
                                  _tappedfemale = false;
                                  genderMale = true;
                                } else {}
                              });
                            },
                            child: Stack(
                              children: <Widget>[
                                Center(
                                  child: Image(
                                      image: AssetImage(
                                          'assets/images/iconfface.png'),
                                      height: 150,
                                      width: 150),
                                ),
                                _tapped
                                    ? Positioned(
                                        top: 0.0,
                                        right: 0.0,
                                        child: FractionalTranslation(
                                          translation: Offset(-1.5, 0.3),
                                          child: checked(),
                                        ),
                                      )
                                    : Text(""),
                              ],
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(bottom: 40),
                              child: Text(
                                "Male",
                                style: Theme.of(context).textTheme.headline5,
                              )),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              print("Female");
                              gender = "Female";
                              setState(() {
                                _tappedfemale = !_tappedfemale;
                                if (_tappedfemale = true) {
                                  genderMale = false;
                                  _tapped = false;
                                } else {}
                              });
                            },
                            child: Stack(
                              children: <Widget>[
                                Center(
                                  child: Image(
                                      image: AssetImage(
                                          'assets/images/avatar_female.png'),
                                      height: 150,
                                      width: 150),
                                ),
                                _tappedfemale
                                    ? Positioned(
                                        top: 0.0,
                                        right: 0.0,
                                        child: FractionalTranslation(
                                          translation: Offset(-1.5, 0.3),
                                          child: checked(),
                                        ),
                                      )
                                    : Text(""),
                              ],
                              //                                Center(
                              // child: ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                              alignment: Alignment.center,
                              child: Text(
                                "Female",
                                style: Theme.of(context).textTheme.headline5,
                              )),
                        ],
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                              splashColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return SetPassword();
                                }));
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    "Next",
                                    style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontSize: 22),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(
                                    Icons.forward,
                                    color: Theme.of(context).accentColor,
                                  )
                                ],
                              )),
                        ))
                  ]),
            )),
      ),
    );
  }

  Widget checked() {
    return CircleAvatar(
      backgroundColor: Colors.transparent,
      child: Image(
        width: 50,
        height: 50,
        fit: BoxFit.cover,
        image: AssetImage("assets/images/ok.png"),
      ),
    );
  }
}

class MyTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hinttext;

  const MyTextField({Key key, this.controller, this.hinttext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(bottom: 15),
        alignment: Alignment.center,
        child: TextFormField(
          style: TextStyle(height: 0.8),
          validator: (value) {
            if (value.isEmpty) {
              return "Field Cannot be Empty";
            } else {
              return null;
            }
          },
          controller: controller,
          decoration: InputDecoration(
              errorStyle: TextStyle(fontSize: 12),
              labelText: hinttext,
              labelStyle: Theme.of(context).textTheme.subtitle2,
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black))),
        ));
  }
}

class SetPassword extends StatefulWidget {
  @override
  _SetPasswordState createState() => _SetPasswordState();
}

class _SetPasswordState extends State<SetPassword> {
  final TextEditingController passwordcontroller = new TextEditingController();
  final TextEditingController usernamecontroller = new TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool obscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(40.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
                    Widget>[
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    //"Welcome Home, " + username,
                    "Hello",
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontSize: 22,
                        fontWeight: FontWeight.bold),
                  )),
              SizedBox(height: 30),
              genderMale
                  ? InkWell(
                      splashColor: Colors.transparent,
                      onTap: () {
                        print("Male");
                      },
                      child: Center(
                          child: Image(
                              image: AssetImage('assets/images/iconfface.png'),
                              height: 100,
                              width: 100)),
                    )
                  : InkWell(
                      splashColor: Colors.transparent,
                      onTap: () {
                        print("Female");
                      },
                      child: Center(
                          child: Image(
                              image:
                                  AssetImage('assets/images/avatar_female.png'),
                              height: 100,
                              width: 100)),
                    ),
              SizedBox(height: 10),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(bottom: 20),
                  child: Text(
                    "Create your Account",
                    style: Theme.of(context).textTheme.headline5,
                  )),
              SizedBox(height: 10),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                        child: MyTextField(
                      controller: usernamecontroller,
                      hinttext: "Username",
                    )),
                    Container(
                        padding: EdgeInsets.only(bottom: 10),
                        alignment: Alignment.center,
                        child: TextFormField(
                          obscureText: obscurePassword,
                          validator: (value) {
                            var data;
                            if (value.isEmpty) {
                              data = "Password cannot be Empty";
                            } else {
                              data = null;
                            }
                            return data;
                          },
                          controller: passwordcontroller,
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    Icons.remove_red_eye,
                                    color: this.obscurePassword
                                        ? Colors.grey
                                        : Theme.of(context).accentColor,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      this.obscurePassword =
                                          !this.obscurePassword;
                                    });
                                  }),
                              errorStyle: TextStyle(fontSize: 12),
                              labelText: "Password",
                              labelStyle: Theme.of(context).textTheme.subtitle2,
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(color: Colors.black))),
                        )),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.center,
                  child: FlatButton(
                      color: Theme.of(context).accentColor,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          createProfile(usernamecontroller.text);
                          username = usernamecontroller.text;
                          savePassword(passwordcontroller.text);
                          profile();
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => HomeBoard()));
                          loadPassword().then(setPassword);
                        } else {}
                      },
                      child: Text(
                        "Create Account",
                        style: TextStyle(color: Colors.white),
                      ))),
            ]),
          ),
        ));
  }

  void setPassword(String value) {
    setState(() {
      password = value;
    });
  }
}

class ViewProfile extends StatefulWidget {
  @override
  _ViewProfileState createState() => _ViewProfileState();
}

class _ViewProfileState extends State<ViewProfile> {
  String _name = "";
  String _email = "";
  String _phone = "";
  String _username = "";
  String _address = "";
  String _dob = "";

  @override
  void initState() {
    readProfile().then(updateProfile);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Profile",
          style: Theme.of(context).appBarTheme.textTheme.headline1,
        ),
        elevation: 0,
        centerTitle: true,
      ),
      body: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Edit Profile",
                    style: TextStyle(color: Colors.black, fontSize: 32),
                  )),
              Container(
                  padding: EdgeInsets.only(top: 10, left: 10),
                  color: Colors.white,
                  child: Align(
                    alignment: Alignment.center,
                    child: CircleAvatar(
                      radius: 80,
                      backgroundImage: AssetImage(
                          "assets/images/2857b4e29ac963e10390745ab48dd882.jpg"),
                    ),
                  )),
              textfield(_name),
              textfield(_username),
              textfield(_email),
              textfield(_phone),
              textfield(_address),
              textfield(_dob),
            ],
          )),
    );
  }

  Widget textfield(String hinttext) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 5),
      child: TextFormField(
          enabled: false,
          decoration: InputDecoration(
              hintText: "$hinttext",
              hintStyle: TextStyle(color: Colors.grey, fontSize: 16))),
    );
  }

  void updateProfile(String value) {
    setState(() {
      this._name = value;
    });
  }
}
