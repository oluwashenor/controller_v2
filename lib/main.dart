import 'package:controller_v2/dialogs.dart';
import 'package:controller_v2/global.dart';
import 'package:controller_v2/rooms.dart';
import 'package:controller_v2/splashscreen.dart';
import 'package:controller_v2/utils/DatabaseHelper.dart';
import 'package:controller_v2/utils/models.dart';
import 'package:controller_v2/utils/sharedpref.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:controller_v2/providers/theme.dart';
import 'apptheme.dart';
import 'my_flutter_app_icons.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(ChangeNotifierProvider<ThemeChanger>(
      create: (_) => ThemeChanger(Apptheme.lighttheme), child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeChanger>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Controller',
      theme: themeChanger.getTheme(),
      home: SplashScreen(),
    );
  }
}

class HomeBoard extends StatefulWidget {
  @override
  _HomeBoardState createState() => _HomeBoardState();
}

var cardAspectRatio = 12.0 / 16.0;
var widgetAspectRatio = cardAspectRatio * 1.2;

class _HomeBoardState extends State<HomeBoard> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController namecontroller = new TextEditingController();

  int id;
  var helperDb;
  bool isUpdating;

  // var currentPage = images.length - 1.0;

  String greet = "";
  String weekDay = "";
  bool day = true;

  updateIP(String value) {
    setState(() {
      address = value;
    });
  }

  // updateTheme(bool value) {
  //   setState(() {
  //     dayTheme = value;
  //   });
  // }

  String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      setState(() {
        day = true;
      });
      return "Good morning";
    } else if (hour < 16) {
      return "Good afternoon";
    }
    setState(() {
      day = false;
    });
    return "Good evening";
  }

  var date = intl.DateFormat('d MMM y').format(DateTime.now());
  var time = int.parse(intl.DateFormat('HH').format(DateTime.now()));

  getRooms() async {
    List<Room> rooms = await DatabaseHelper.db.allRooms();
    roomslist = [];
    setState(() {
      rooms.forEach((element) {
        roomslist.add(element);
      });
    });
  }

  updateUiForRoom() {
    getRooms();
  }

  updateUiforButtons() async {
    getButtons();
  }

  getButtons() async {
    List<Button> buttons = await DatabaseHelper.db.allButtons();
    buttonslist = [];
    setState(() {
      buttons.forEach((element) {
        buttonslist.add(element);
        print(element);
      });
    });
  }

  TextEditingController componentnamecontroller = new TextEditingController();
  TextEditingController pincontroller = new TextEditingController();
  Future<int> test;

  void refreshView() {
    updateUiForRoom();
    setState(() {});
  }

  void refreshButtons() {
    updateUiforButtons();
    setState(() {});
  }

  @override
  void initState() {
    loadIP().then(updateIP);
    super.initState();
    greet = greeting();
    test = DatabaseHelper.db.buttonsQuantity();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
                icon: Icon(Icons.add),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return IPDialog();
                      });
                }),
          )
        ],
        elevation: 0,
        title: Text(
          "Home",
          style: Theme.of(context).appBarTheme.textTheme.headline1,
        ),
        centerTitle: true,
      ),
      drawer: Drawer(child: MyDrawer()),
      body: SafeArea(
        child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).appBarTheme.color,
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(30),
                    ),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 18),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                    child: RichText(
                                  text: TextSpan(
                                      text: "Hello, ",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: username,
                                            style: Theme.of(context)
                                                .textTheme
                                                .subtitle1),
                                      ]),
                                )),
                              ],
                            ),
                            Container(
                              child: Text(
                                "$date",
                                style: Theme.of(context).textTheme.subtitle2,
                              ),
                            )
                          ]),
                      Row(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.only(top: 2, right: 3),
                              child: Text("$greet",
                                  style:
                                      Theme.of(context).textTheme.subtitle2)),
                          Padding(
                            padding: const EdgeInsets.only(left: 3.0),
                            child: day
                                ? Icon(
                                    MyFlutterApp.cloud_sun,
                                    color: yellowcolor,
                                    size: 25,
                                  )
                                : Icon(MyFlutterApp.cloud_moon,
                                    size: 25, color: Colors.blueGrey),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0, bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 3.0),
                                  child: Material(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(05),
                                      ),
                                      color: Theme.of(context).accentColor,
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(MyFlutterApp.temperature,
                                            size: 15, color: Colors.white),
                                      )),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text("Temperature 30\u{2103} ",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2)),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 0.0),
                                  child: Material(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(05),
                                      ),
                                      color: Theme.of(context).accentColor,
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(MyFlutterApp.humidity_1_,
                                            size: 15, color: Colors.white),
                                      )),
                                ),
                                Container(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text("Humidity 90%",
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle2)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 15.0, right: 8.0, left: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 10.0, bottom: 2),
                            child: Text(
                              "Rooms",
                              style: Theme.of(context).textTheme.subtitle2,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5, bottom: 2),
                            child: Material(
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(5.0),
                                ),
                                color: Theme.of(context).accentColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(3.0),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    size: 10,
                                    color: Colors.white,
                                  ),
                                )),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 13.0),
                        child: GestureDetector(
                          onTap: () {
                            displayModalBottomSheet(context, "room");
                          },
                          child: Icon(Icons.add, size: 20),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  child: roomslist.isEmpty
                      ? Center(
                          child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                              "Welcome $username, You have not created a room, start by tapping on the \"+\" icon above",
                              style:
                                  Theme.of(context).primaryTextTheme.headline3,
                              textAlign: TextAlign.center),
                        ))
                      : GridView.builder(
                          shrinkWrap: true,
                          itemCount: roomslist.length,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return RoomCard(
                              notifyParent: refreshView,
                              index: index,
                              room: roomslist[index],
                            );
                          }),
                )),
                Container(
                  //height: 120,
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0, top: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, bottom: 2),
                                  child: Text(
                                    "Quick actions",
                                    style:
                                        Theme.of(context).textTheme.subtitle2,
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5, bottom: 2),
                                  child: Material(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(5.0),
                                      ),
                                      color: Theme.of(context).accentColor,
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          size: 10,
                                          color: Colors.white,
                                        ),
                                      )),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 13.0),
                              child: GestureDetector(
                                onTap: () {
                                  displayModalBottomSheet(context, "button");
                                },
                                child: Icon(Icons.add, size: 20),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 3),
                        child: buttonslist.isNotEmpty
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: buttonslist.map(button).toList(),
                              )
                            : Container(
                                height: 60,
                                child: Center(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 7.0),
                                    child: Text(
                                        "Your Quick Action Buttons are empty, Tap on the  \"+\" above to begin",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline3,
                                        textAlign: TextAlign.center),
                                  ),
                                )),
                      )
                    ],
                  ),
                )
              ],
            )),
      ),
    );
  }

  deleteRoomConfirmation(int index) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return DeleteRoomDialog(index: index, notifyParent: refreshView);
        });
  }

  Widget button(
    Button button,
  ) {
    String pin = button.pin;
    int codePoint = int.parse(button.icon);
    return GestureDetector(
      onLongPress: () {
        configureDialog(button);
      },
      child: Material(
          color: button.state
              ? Colors.orange.shade200
              : Theme.of(context).appBarTheme.color,
          elevation: button.state ? 5 : 0,
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(20.0),
          ),
          child: InkWell(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onTap: () async {
              print(button.type);
              if (button.type == "button") {
                print("button");
                print(button.icon);
                setState(() {
                  button.state = !button.state;
                });
                print("here");
                print(button.state);
                String val = "";

                switch (button.state) {
                  case true:
                    val = "ON";
                    print(address);
                    String add =
                        "http://" + address + "/device/pin" + pin + "=" + val;
                    var url = Uri.http(add, "");
                    print("My Address is " + add);

                    try {
                      var response = await http.get(url);
                      print("This is Response ${response.statusCode}");
                      DatabaseHelper.db.updateButtonState(1, button.id);
                      print("Pin ${button.pin} is Switched $val");
                    } catch (e) {
                      print("Pin ${button.pin} failed to Switch $val");
                      print(e);
                      setState(() {
                        button.state = !button.state;
                      });
                    }
                    break;
                  default:
                    val = "OFF";
                    String add =
                        "http://" + address + "/device/pin" + pin + "=" + val;
                    var url = Uri.http(add, "");
                    print(add);

                    try {
                      var response = await http.get(url);
                      print(response.statusCode);
                      DatabaseHelper.db.updateButtonState(0, button.id);
                      print("Pin ${button.pin} is Switched $val");
                    } catch (e) {
                      print("Pin ${button.pin} failed to Switch $val");
                      setState(() {
                        button.state = !button.state;
                      });
                    }
                }
              } else {
                print("custom");
                print(button.icon);
                setState(() {
                  button.state = !button.state;
                });
                String val = "";

                switch (button.state) {
                  case true:
                    val = "open";
                    // var add = "http://" + address + "/" + val + pin;
                    var add = val + pin;
                    var url = Uri.http(address, add);
                    print(add);
                    print("AM here");

                    try {
                      var response = await http.get(url);
                      print("This is Response ${response.statusCode}");
                      DatabaseHelper.db.updateButtonState(1, button.id);
                      print("Pin ${button.pin} is Switched $val");
                    } catch (e) {
                      print("Pin ${button.pin} failed to Switch $val");
                      print(e);
                      setState(() {
                        button.state = !button.state;
                      });
                    }
                    break;
                  default:
                    val = "close";
                    // var add = "http://" + address + "/" + val + pin;
                    var add = val + pin;
                    var url = Uri.http(address, add);
                    print(add);
                    print("AM also here");

                    try {
                      var response = await http.get(url);
                      print(response.statusCode);
                      DatabaseHelper.db.updateButtonState(0, button.id);
                      print("Pin ${button.pin} is Switched $val");
                    } catch (e) {
                      print("Pin ${button.pin} failed to Switch $val");
                      setState(() {
                        button.state = !button.state;
                      });
                    }
                }
              }
            },
            child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Icon(IconData(codePoint, fontFamily: "MyFlutterApp"))),
          )),
    );
  }

  displayModalBottomSheet(BuildContext context, String destination) {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext context) {
          if (destination == "room") {
            return RoomButtomSheet(notifyParent: refreshView);
          } else {
            return ButtonButtomSheet(notifyParent: refreshButtons);
          }
        });
  }

  // configureDialog(Button button) {
  //   showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return ConfigureDialog(button: button);
  //       });
  // }

  configureDialog(Button button) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
              titlePadding: EdgeInsets.all(0),
              contentPadding: EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              children: <Widget>[
                Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Container(
                    color: Colors.transparent,
                    padding: EdgeInsets.only(top: 10, left: 20),
                    alignment: Alignment.center,
                    child: Text(
                      "Configure ${button.name}",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  Form(
                      child: Column(children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: textfield(namecontroller, "Name"),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: textfield(pincontroller, "${button.pin}"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: RaisedButton(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    color: Theme.of(context).accentColor,
                                  ),
                                  borderRadius: BorderRadius.circular(5)),
                              color: Theme.of(context).appBarTheme.color,
                              onPressed: () {
                                //  _pickIcon();
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 18),
                                child: Row(
                                  children: [
                                    Text(
                                      // _icon != null
                                      //     ? "Change Icon"
                                      "Select Icon",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    // AnimatedSwitcher(
                                    //     duration: Duration(milliseconds: 500),
                                    //     child: _icon != null
                                    //         ? Icon(iconData)
                                    //         : Container()),
                                  ],
                                ),
                              )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: RaisedButton(
                              elevation: 1,
                              color: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              onPressed: () {
                                DatabaseHelper.db.deleteButton(button.id);
                                //    buttonslist.removeAt(index);
                                // setState(() {});
                                updateUiforButtons();

                                Navigator.pop(context);
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 20.0, horizontal: 10),
                                child: Text(
                                  "Delete Button",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: RaisedButton(
                              elevation: 1,
                              color: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              onPressed: () {
                                print("New pin is " + pincontroller.text);
                                var updateDBButton = Button(
                                  pin: pincontroller.text,
                                );
                                DatabaseHelper.db
                                    .updateButton(updateDBButton, button.id);
                                updateUiforButtons();
                                Navigator.pop(context);
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 20.0, horizontal: 10),
                                child: Text(
                                  "Update Button",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                  ])),
                ])
              ]);
        });
  }

  Widget textfield(TextEditingController c, String name) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        child: TextFormField(
          controller: c,
          decoration: InputDecoration(
              labelText: name,
              labelStyle: Theme.of(context).textTheme.subtitle2,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black))),
        ));
  }
}
