import 'package:controller_v2/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class Chatroom extends StatelessWidget {
  final TextEditingController chatcontroller = new TextEditingController();

  List chatList = ["Adeshina"];

  Future<http.Response> chatter(String add) async {
    print("My address " + add);
    //  newaddress = address
    var url = Uri.http(address, add);
    print("Url" + url.toString());
    print(chatList);
    try {
      return await http.get(url);
    } catch (e) {
      print(e);
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        //  mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Expanded(
            child: Container(
              color: Colors.white,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            alignment: Alignment.bottomCenter,
            width: double.infinity * 0.9,
            child: TextFormField(
              onFieldSubmitted: (value) {
                value = value.replaceAll(" ", "");
                print(value);
                String add = value;
                chatList.add(add);
                chatter(add);
              },
              controller: chatcontroller,
              decoration: InputDecoration(
                  labelText: "Send a message ........",
                  labelStyle: Theme.of(context).textTheme.subtitle2,
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide:
                          BorderSide(color: Theme.of(context).accentColor)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: Colors.black))),
            ),
          ),
        ],
      ),
    );
  }
}
