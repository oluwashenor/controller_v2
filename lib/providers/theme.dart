import 'package:flutter/material.dart';

import '../apptheme.dart';

class ThemeChanger extends ChangeNotifier {
  ThemeData _themeData = Apptheme.lighttheme;

  ThemeChanger(this._themeData);

  getTheme() => _themeData;

  setTheme(ThemeData theme) {
    _themeData = theme;
    notifyListeners();
  }
}
