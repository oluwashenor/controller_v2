import 'package:controller_v2/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Boarding extends StatefulWidget {
  @override
  _BoardingState createState() => _BoardingState();
}

class _BoardingState extends State<Boarding> {
  final int _numPages = 3;

  PageController _pageController = PageController(initialPage: 0);

  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8),
      height: 8,
      width: isActive ? 24 : 16,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: isActive ? Theme.of(context).accentColor : Colors.grey),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 40.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                      alignment: Alignment.centerRight,
                      child: TextButton(
                        onPressed: () {
                          print("Skip");
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => Gender()));
                        },
                        child: Text(
                          "Skip",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 20),
                        ),
                      )),
                  Container(
                    height: 500,
                    child: PageView(
                      physics: ClampingScrollPhysics(),
                      onPageChanged: (int page) {
                        setState(() {
                          _currentPage = page;
                        });
                      },
                      controller: _pageController,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                        child: Center(
                                            child: Image(
                                                image: AssetImage(
                                                    'assets/images/undraw_smart_home_28oy.png'),
                                                height: 350,
                                                width: 300)),
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 30),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  "Welcome Home ",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5,
                                                  textAlign: TextAlign.start,
                                                )),
                                            SizedBox(height: 10),
                                            Text(
                                              "Welcome to your Smart's Home Mobile Control Interface, Relax and Enjoy.",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2,
                                            ),
                                          ],
                                        ),
                                      ))
                                ]),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                        child: Center(
                                            child: Image(
                                                image: AssetImage(
                                                    'assets/images/undraw_maker_launch_crhe.png'),
                                                height: 350,
                                                width: 300)),
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 30),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  "Fast & Lightweight",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5,
                                                  textAlign: TextAlign.start,
                                                )),
                                            SizedBox(height: 10),
                                            Text(
                                              "The Application is designed to be very fast and built to run efficiently on all platforms and devices.",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2,
                                            ),
                                          ],
                                        ),
                                      ))
                                ]),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Expanded(
                                      flex: 3,
                                      child: Container(
                                        child: Center(
                                            child: Image(
                                                image: AssetImage(
                                                    'assets/images/undraw_personalization_triu.png'),
                                                height: 350,
                                                width: 300)),
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 30),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                  "Customizable",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .headline5,
                                                  textAlign: TextAlign.start,
                                                )),
                                            SizedBox(height: 10),
                                            Text(
                                              "The Mobile Application is designed to be easy and fully customizable, you can create custom rooms, configurations",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle2,
                                            ),
                                          ],
                                        ),
                                      ))
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: _buildPageIndicator()),
                  _currentPage != _numPages - 1
                      ? Expanded(
                          child: Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                              splashColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: () {
                                _pageController.nextPage(
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(top: 40.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      "Next",
                                      style: TextStyle(
                                          color: Theme.of(context).accentColor,
                                          fontSize: 22),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Icon(
                                      Icons.forward,
                                      color: Theme.of(context).accentColor,
                                    )
                                  ],
                                ),
                              )),
                        ))
                      : Text(''),
                ]),
          ),
        ),
      ),
      bottomSheet: _currentPage == _numPages - 1
          ? Container(
              height: 80,
              width: double.infinity,
              color: Theme.of(context).accentColor,
              child: GestureDetector(
                onTap: () {
                  print("Get Started");
                  // Navigator.of(context).pushReplacement(
                  //     MaterialPageRoute(builder: (context) => Gender()));
                },
                child: Center(
                    child: Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    "Start Setting up",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                )),
              ),
            )
          : Text(''),
    );
  }
}
