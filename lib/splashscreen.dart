import 'dart:async';
import 'package:controller_v2/utils/DatabaseHelper.dart';
import 'package:controller_v2/utils/models.dart';
import 'package:controller_v2/utils/sharedpref.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'global.dart';
import 'lockscreen.dart';
import 'onboarding.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    getButton();
    getRooms();
    loadPassword().then(setPassword);
    firsttime();
  }

  getButton() async {
    List<Button> buttons = await DatabaseHelper.db.allButtons();

    setState(() {
      buttons.forEach((element) {
        buttonslist.add(element);
      });
    });
  }

  getRooms() async {
    List<Room> rooms = await DatabaseHelper.db.allRooms();

    setState(() {
      rooms.forEach((element) {
        roomslist.add(element);
      });
    });
  }

  firsttime() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool profile = prefs.getBool("profile");

    var _duration = new Duration(seconds: 6);
    if (profile != null && profile == true) {
      return new Timer(_duration, navigationlockscreen);
    } else {
      return new Timer(_duration, navigationonboarding);
    }
  }

  void navigationlockscreen() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => LockScreen(),
    ));
  }

  void navigationonboarding() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => Boarding(),
    ));
  }

  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      height: double.infinity,
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SpinKitCircle(
                  color: Theme.of(context).accentColor,
                  size: 50,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("\nSetting up your home",
                    style: Theme.of(context).primaryTextTheme.caption)
              ],
            ),
          ]),
    );
  }

  void setPassword(String value) {
    setState(() {
      password = value;
    });
  }

  void setButtonsNumber(int value) {
    setState(() {
      numberOfButtons = value;
    });
  }
}
