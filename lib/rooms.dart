import 'package:controller_v2/dialogs.dart';
import 'package:controller_v2/utils/DatabaseHelper.dart';
import 'package:controller_v2/utils/data.dart';
import 'package:controller_v2/utils/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'global.dart';
import 'package:http/http.dart' as http;
import 'my_flutter_app_icons.dart';

class Rooms extends StatefulWidget {
  final String title;
  final int roomId;

  const Rooms({Key key, this.title, this.roomId}) : super(key: key);
  @override
  _RoomsState createState() => _RoomsState();
}

class _RoomsState extends State<Rooms> {
  TextEditingController namecontroller = new TextEditingController();
  TextEditingController pincontroller = new TextEditingController();

  final _formKey = GlobalKey<FormState>();

  getDevices() async {
    List<Device> devices =
        await DatabaseHelper.db.allRoomDevices(widget.roomId);
    deviceslist = [];
    setState(() {
      devices.forEach((element) {
        deviceslist.add(element);
      });
    });
  }

  updateUiForDevices() {
    getDevices();
  }

  @override
  void initState() {
    super.initState();
    loadDevices = getDevices();
  }

  @override
  Widget build(BuildContext context) {
    var screenlength = MediaQuery.of(context).size.height;
    return FutureBuilder(
        future: loadDevices,
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.none) {
            return Center(
              child: Text("Unable to load Room"),
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
                appBar: AppBar(
                  actions: <Widget>[
                    Builder(
                      builder: (context) => IconButton(
                          icon: Icon(Icons.settings), onPressed: () {}),
                    )
                  ],
                  elevation: 0,
                  title: Text(
                    "${widget.title}",
                    style: Theme.of(context).appBarTheme.textTheme.headline1,
                  ),
                  centerTitle: true,
                ),
                body: Container(
                  child: Column(children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Theme.of(context).appBarTheme.color,
                        borderRadius: BorderRadius.vertical(
                          bottom: Radius.circular(30),
                        ),
                      ),
                      width: double.infinity,
                      //height: screenlength * 0.3,
                      height: screenlength * 0.13,
                      child: Column(children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(20),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      //crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(1.0),
                                              child: Icon(
                                                  MyFlutterApp.temperature,
                                                  size: 15,
                                                  color: Colors.black),
                                            ),
                                            Text(
                                              "19\u{2103} ",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1,
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Temperature",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline4,
                                        )
                                        // 00B0
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      //crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 0.0),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(1.0),
                                                child: Icon(
                                                    Icons.lightbulb_outline,
                                                    size: 15,
                                                    color: Colors.black),
                                              ),
                                            ),
                                            Text(
                                              "85 %",
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .subtitle1,
                                            ),
                                          ],
                                        ),
                                        Text(
                                          "Temperature",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline4,
                                        )
                                        // 00B0
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        //crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(1.0),
                                                child: Icon(
                                                    MyFlutterApp.humidity_1_,
                                                    size: 15,
                                                    color: Colors.black),
                                              ),
                                              Text(
                                                "85 %",
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .subtitle1,
                                              ),
                                            ],
                                          ),
                                          Text(
                                            "Humidity",
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline4,
                                          )
                                          // 00B0
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ]),
                    ),
                    Container(
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 10, right: 10, left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10.0, bottom: 2),
                                  child: Text(
                                    "Devices",
                                    style:
                                        Theme.of(context).textTheme.subtitle2,
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5, bottom: 2),
                                  child: Material(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(5.0),
                                      ),
                                      color: Theme.of(context).accentColor,
                                      child: Padding(
                                        padding: const EdgeInsets.all(3.0),
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          size: 10,
                                          color: Colors.white,
                                        ),
                                      )),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 13.0),
                              child: GestureDetector(
                                onTap: () {
                                  displayModalBottomSheet(context);
                                },
                                child: Icon(Icons.add,
                                    size: 20, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: GridView.builder(
                        scrollDirection: Axis.vertical,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2),
                        // shrinkWrap: true,
                        //physics: NeverScrollableScrollPhysics(),
                        itemCount: deviceslist.length,
                        itemBuilder: (BuildContext context, index) {
                          return deviceCard(index);
                        },
                      ),
                    ),
                  ]),
                ));
          } else {
            return Center(
              child: SpinKitCircle(
                color: Theme.of(context).accentColor,
                size: 50,
                // type: SpinKitWaveType.start,
              ),
            );
          }
        });
  }

  void displayModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) {
          return createDeviceModal();
        });
  }

  Widget createDeviceModal() {
    return Container(
        height: MediaQuery.of(context).size.height / 2.1 +
            MediaQuery.of(context).viewInsets.bottom,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
          color: Theme.of(context).appBarTheme.color,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: Colors.transparent,
              padding: EdgeInsets.only(top: 30, left: 20),
              alignment: Alignment.bottomLeft,
              child: Text(
                "Create new Device",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                      key: _formKey,
                      child: Column(children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: textfield(namecontroller, "Name"),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: textfield(pincontroller, "Pin"),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Expanded(
                              child: ElevatedButton(
                                  // shape: RoundedRectangleBorder(
                                  //     side: BorderSide(
                                  //       color: Theme.of(context).accentColor,
                                  //     ),
                                  //     borderRadius: BorderRadius.circular(5)),
                                  // color: Theme.of(context).appBarTheme.color,
                                  onPressed: () {
                                    deviceImageDialog();
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0, vertical: 20),
                                    child: Text(
                                      "Select Image",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                  )),
                            ),
                            SizedBox(width: 20),
                            Expanded(
                              child: ElevatedButton(
                                  // elevation: 1,
                                  // color: Theme.of(context).accentColor,
                                  // shape: RoundedRectangleBorder(
                                  //     borderRadius: BorderRadius.circular(5)),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      if (selectedRoomImage != null) {
                                        var newDevice = Device(
                                            pin: pincontroller.text,
                                            name: namecontroller.text,
                                            roomId: widget.roomId,
                                            icon: selectedRoomImage,
                                            state: false);
                                        DatabaseHelper.db.newDevice(newDevice);
                                        namecontroller.clear();
                                        pincontroller.clear();
                                        selectedRoomImage = null;
                                        setState(() {});
                                        updateUiForDevices();
                                        Navigator.pop(context);
                                      } else {
                                        deviceImageDialog();
                                      }
                                    }
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 20.0, horizontal: 10),
                                    child: Text(
                                      "Create Button",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      ])),
                )
              ],
            )
          ],
        ));
  }

  Widget textfield(TextEditingController c, String name) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        child: TextFormField(
          controller: c,
          decoration: InputDecoration(
              labelText: name,
              labelStyle: Theme.of(context).textTheme.subtitle2,
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black))),
        ));
  }

  Widget deviceCard(int index) {
    String pin = deviceslist[index].pin;
    return GestureDetector(
      onTap: () {
        print("Device Tapped");
      },
      onLongPress: () {
        DatabaseHelper.db.deleteDevice(deviceslist[index].id);
        deviceslist.removeAt(index);
        setState(() {});
      },
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            color: Theme.of(context).appBarTheme.color,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Padding(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) => Vary(pin: pin),
                          );
                        },
                        // child: Icon(Icons.lightbulb_outline, color: Colors.black
                        // )
                        child: SvgPicture.asset(
                            "assets/svgicons/${deviceslist[index].icon}",
                            semanticsLabel: "Logo",
                            width: 40,
                            height: 40),
                      ),
                    ]),
                    Switch(
                      value: deviceslist[index].state,
                      onChanged: (value) {
                        String val = "";
                        print("double wahala");
                        setState(() {
                          bool buttonstate = value;

                          deviceslist[index].state = value;
                          if (buttonstate == true) {
                            val = "ON";
                            String add = "/device/pin" + pin + "/" + val;
                            print(add);
                            var url = Uri.http(address, add);
                            http.get(url);
                            print(url);
                            print(
                                "${deviceslist[index].name} in ${widget.title} is on");
                          } else {
                            val = "OFF";
                            String add = "/device/pin" + pin + "/" + val;
                            print(add);
                            var url = Uri.http(address, add);
                            http.get(url);
                            print(url);
                            print(
                                "${deviceslist[index].name} in ${widget.title} is Off");
                          }
                        });
                      },
                      activeTrackColor: Theme.of(context).accentColor,
                      activeColor: Theme.of(context).accentColor,
                    ),
                  ],
                ),
                Row(children: <Widget>[
                  Text("${deviceslist[index].name}",
                      style: Theme.of(context).textTheme.subtitle2),
                ]),
                deviceslist[index].state
                    ? Text("On", style: Theme.of(context).textTheme.subtitle2)
                    : Text("Off", style: Theme.of(context).textTheme.subtitle2),
              ],
            ),
          ),
        ),
      ),
    );
  }

  deviceImageDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
              titlePadding: EdgeInsets.all(0),
              contentPadding: EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              children: <Widget>[
                Container(
                  height: 500,
                  child: GridView.builder(
                    scrollDirection: Axis.vertical,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2),
                    itemCount: deviceImages.length,
                    itemBuilder: (BuildContext context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            print("image selected is ${deviceImages[index]}");
                            selectedRoomImage = "${deviceImages[index]}";
                            print(selectedRoomImage);
                            Navigator.pop(context);
                          },
                          child: SvgPicture.asset(
                              "assets/svgicons/${deviceImages[index]}",
                              semanticsLabel: "Logo"),
                        ),
                      );
                    },
                  ),
                )
              ]);
        });
  }
}

class Vary extends StatefulWidget {
  final String pin;
  final bool state;

  const Vary({Key key, this.pin, this.state}) : super(key: key);
  @override
  _VaryState createState() => _VaryState();
}

class _VaryState extends State<Vary> {
  String val;
  double sliderValue = 0.0;
  double sliderValue2 = 0.0;
  var oldSliderValue;
  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        child: Container(
          height: 100,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Slider(
                  min: 0,
                  max: 255,
                  value: sliderValue,
                  divisions: 17,
                  onChanged: (value) {
                    setState(() {
                      oldSliderValue = sliderValue;
                      sliderValue = value;
                      print(sliderValue);

                      String val = sliderValue.toString();
                      String add = "http://" +
                          address +
                          "/device/pin" +
                          widget.pin +
                          "=" +
                          val;
                      // http.get(add);
                    });
                  },
                )
              ],
            ),
          ),
        ));
  }
}

class RoomCard extends StatelessWidget {
  final Room room;
  final int index;
  final Function() notifyParent;
  const RoomCard({Key key, this.room, this.index, this.notifyParent})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    String roomname = room.name;
    int roomId = room.id;
    String image = room.icon;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      padding: EdgeInsets.symmetric(horizontal: 1, vertical: 1),
      child: GestureDetector(
        onTap: () {
          DatabaseHelper.db.roomDevicesQuantity(roomId);
          print("Tapped on $roomname, with this ID $roomId");
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Rooms(
              title: roomname,
              roomId: roomId,
            ),
          ));
        },
        onLongPress: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DeleteRoomDialog(
                  index: index,
                  notifyParent: notifyParent,
                );
              });
        },
        child: new Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          color: Theme.of(context).appBarTheme.color,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SvgPicture.asset("assets/svgicons/$image",
                    semanticsLabel: "Logo", width: 95, height: 95),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        roomname,
                        style: Theme.of(context).primaryTextTheme.caption,
                      )),
                )
              ]),
        ),
      ),
    );
  }
}
