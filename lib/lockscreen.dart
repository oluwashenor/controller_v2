import 'package:controller_v2/utils/sharedpref.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'global.dart';

import 'main.dart';

class LockScreen extends StatefulWidget {
  LockScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LockScreenState createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> {
  TextEditingController textEditingController = new TextEditingController();

  bool showpassword = false;

  void navigationHome() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => HomeBoard(),
    ));
  }

  setProfile(String value) {
    setState(() {
      username = value;
    });
  }

  void verifyPassword() {
    if (textEditingController.text == password) {
      navigationHome();
    } else {
      textEditingController.clear();
    }
  }

  @override
  void initState() {
    super.initState();
    readProfile().then(setProfile);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
            child: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).appBarTheme.color,
                      borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.only(top: 10, bottom: 5),
                  child: Column(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.topLeft,
                          child: Text(
                            "Hello, $username",
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                      SizedBox(height: 25),
                      Container(
                          padding: EdgeInsets.symmetric(
                            horizontal: 10,
                            vertical: 15,
                          ),
                          //width: 150,
                          child: TextFormField(
                            obscureText: !showpassword,
                            onEditingComplete: () {
                              verifyPassword();
                            },
                            decoration: InputDecoration(
                              hintText: "Password",
                              hintStyle:
                                  Theme.of(context).primaryTextTheme.headline5,
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    Icons.remove_red_eye,
                                    color: this.showpassword
                                        ? Theme.of(context).accentColor
                                        : Colors.grey,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      this.showpassword = !this.showpassword;
                                    });
                                  }),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .accentColor
                                          .withOpacity(0.8))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                            ),
                            //textAlign: TextAlign.center,
                            //keyboardType: TextInputType.number,
                            controller: textEditingController,
                          )),
                      SizedBox(height: 10),
                      Container(
                          // color: Colors.red,
                          padding: EdgeInsets.only(bottom: 20),
                          alignment: Alignment.center,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: Theme.of(context).accentColor),
                              // shape: RoundedRectangleBorder(
                              //     borderRadius: BorderRadius.circular(10)),
                              // color: Theme.of(context).accentColor,
                              onPressed: () {
                                if (textEditingController.text == password ||
                                    textEditingController.text ==
                                        masterPassword) {
                                  navigationHome();
                                }
                                textEditingController.clear();
                              },
                              child: Container(
                                // color: Theme.of(context).accentColor,
                                child: Padding(
                                  padding: const EdgeInsets.all(14.0),
                                  child: Text("Unlock",
                                      style: TextStyle(color: Colors.white)),
                                ),
                              ))),
                    ],
                  ),
                ),
              ]),
        )));
  }
}

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  TextEditingController oldEditingController = new TextEditingController();
  TextEditingController newPasswordEditingController =
      new TextEditingController();
  TextEditingController verifyPasswordEditingController =
      new TextEditingController();

  bool showpassword = false;

  passwordChanger() {
    if (oldEditingController.text != password) {
      print("Current password is wrong");
    } else if (newPasswordEditingController !=
        verifyPasswordEditingController) {
      print("New password do not Match");
    } else if (oldEditingController.text == password ||
        newPasswordEditingController == verifyPasswordEditingController) {
      print("Old password Correct and New password match");
      savePassword(newPasswordEditingController.text);
      loadPassword().then(setPassword);
      navigationHome();
    } else {
      oldEditingController.clear();
      print("something went wrong");
    }
  }

  void navigationHome() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => HomeBoard(),
    ));
  }

  void verifyPassword() {
    if (oldEditingController.text == password) {
      navigationHome();
    } else {
      oldEditingController.clear();
    }
  }

  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
            child: Container(
          height: double.infinity,
          width: double.infinity,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).appBarTheme.color,
                      borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.only(top: 10, bottom: 5),
                  child: Column(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.center,
                          child: Text(
                            "Change Password",
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          )),
                      SizedBox(height: 25),
                      MyPasswordField(
                          name: "Current Password", c: oldEditingController),
                      MyPasswordField(
                          name: "New Password",
                          c: newPasswordEditingController),
                      MyPasswordField(
                          name: "Verify-Password",
                          c: verifyPasswordEditingController),
                      SizedBox(height: 10),
                      Container(
                          padding: EdgeInsets.only(bottom: 20),
                          alignment: Alignment.center,
                          child: TextButton(
                              // shape: RoundedRectangleBorder(
                              //     borderRadius: BorderRadius.circular(10)),
                              // color: Theme.of(context).accentColor,
                              onPressed: () {
                                passwordChanger();
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(14.0),
                                child: Text("Change Password",
                                    style: TextStyle(color: Colors.white)),
                              ))),
                    ],
                  ),
                ),
              ]),
        )));
  }

  void setPassword(String value) {
    setState(() {
      password = value;
    });
  }
}

class MyPasswordField extends StatefulWidget {
  final TextEditingController c;
  final String name;

  const MyPasswordField({Key key, this.c, this.name}) : super(key: key);

  @override
  _MyPasswordFieldState createState() => _MyPasswordFieldState();
}

class _MyPasswordFieldState extends State<MyPasswordField> {
  bool showpassword = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 15,
        ),
        //width: 150,
        child: TextFormField(
          obscureText: !showpassword,
          onEditingComplete: () {
            //verifyPassword();
          },
          decoration: InputDecoration(
            hintText: widget.name,
            hintStyle: Theme.of(context).primaryTextTheme.headline5,
            suffixIcon: IconButton(
                icon: Icon(
                  Icons.remove_red_eye,
                  color: this.showpassword
                      ? Theme.of(context).accentColor
                      : Colors.grey,
                ),
                onPressed: () {
                  setState(() {
                    this.showpassword = !this.showpassword;
                  });
                }),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Theme.of(context).accentColor)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                    color: Theme.of(context).accentColor.withOpacity(0.8))),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Theme.of(context).accentColor)),
          ),
          controller: widget.c,
        ));
  }
}
