import 'package:controller_v2/lockscreen.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          "Settings",
          style: Theme.of(context).appBarTheme.textTheme.headline1,
        ),
        elevation: 0,
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            padding: EdgeInsets.symmetric(horizontal: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                listtile(Icon(Icons.network_wifi), "Change Password"),
                listtile(Icon(Icons.network_wifi), "Configure Network"),
                listtile(Icon(Icons.supervised_user_circle), "Profile"),
                ListTile(
                  leading: Icon(Icons.router,
                      color: Theme.of(context).iconTheme.color),
                  title: Text(
                    "Configure IP Address",
                    style: Theme.of(context).primaryTextTheme.subtitle1,
                  ),
                ),
                listtile(Icon(Icons.verified_user), "Version"),
                listtile(Icon(Icons.vertical_align_bottom), "Privacy Policy"),
              ],
            )),
      ),
    );
  }

  Widget listtile(Widget icon, String title) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ChangePasswordScreen(),
        ));
      },
      child: ListTile(
        dense: true,
        leading: Icon(Icons.settings, color: Theme.of(context).iconTheme.color),
        title: Text(
          title,
          style: Theme.of(context).primaryTextTheme.subtitle1,
        ),
      ),
    );
  }
}
