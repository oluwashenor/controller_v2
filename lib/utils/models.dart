import 'dart:convert';

Button buttonFromJson(String str) => Button.fromJson(json.decode(str));

class Button {
  int id;
  String type;
  String pin;
  String icon;
  String name;
  bool state;

  Button({this.id, this.type, this.pin, this.icon, this.name, this.state});

  factory Button.fromJson(Map<String, dynamic> json) => Button(
      id: json['id'],
      type: json['type'],
      pin: json['pin'],
      icon: json['icon'],
      name: json['name'],
      state: json['state'] == 0);

  Map<String, dynamic> toJson() => {
        'id': id,
        'pin': pin,
        'icon': icon,
        'type': type,
        'name': name,
        'state': state == true ? 1 : 0
      };
}

Device deviceFromJson(String str) => Device.fromJson(json.decode(str));

class Device {
  int id;
  String pin;
  String icon;
  String name;
  int roomId;
  bool state;

  Device({this.id, this.pin, this.icon, this.name, this.roomId, this.state});

  factory Device.fromJson(Map<String, dynamic> json) => Device(
      id: json['id'],
      pin: json['pin'],
      icon: json['icon'],
      name: json['name'],
      roomId: json['room_id'],
      state: json['state'] == 0);

  Map<String, dynamic> toJson() => {
        'id': id,
        'pin': pin,
        'icon': icon,
        'name': name,
        'room_id': roomId,
        'state': state == true ? 1 : 0
      };
}

Room roomFromJson(String str) => Room.fromJson(json.decode(str));

class Room {
  int id;
  String name;
  String icon;

  Room({this.id, this.name, this.icon});

  factory Room.fromJson(Map<String, dynamic> json) =>
      Room(id: json['room_id'], name: json['name'], icon: json['icon']);

  Map<String, dynamic> toJson() => {'room_id': id, 'name': name, 'icon': icon};
}
