import 'package:controller_v2/my_flutter_app_icons.dart';
import 'package:flutter/material.dart';

List<String> images = [
  "assets/images/dbd073f5234e3d464c44cf1692429a43.jpg",
  "assets/images/9cd9153e7e0cebf8dd6a82355712f9ec.jpg",
  "assets/images/617596043aabeb1b37c3f79ed6d1908f.jpg",
  "assets/images/0031afe13bcbaf3b19f059f19416b19f.jpg"
];

List<String> title = ["Bathroom", "Kitchen", "Bedroom", "Shower"];

List<String> assetimages = [
  "baby-bath-tub.svg",
  "bath-tub.svg",
  "bed.svg",
  "clean.svg",
  "cooking_chef.svg",
  "cooking.svg",
  "dinner(1).svg",
  "dinning-table.svg",
  "dinning-table(1).svg",
  "free pick bed.svg",
  "living-room.svg",
  "romantic-dinner.svg",
  "room goodware.svg",
  "shower.svg",
  "toilet.svg",
];

List<Icon> myIcons = [
  Icon(MyFlutterApp.cancel),
  Icon(MyFlutterApp.cloud_moon),
  Icon(MyFlutterApp.cloud_sun),
  Icon(MyFlutterApp.heat),
  Icon(MyFlutterApp.home),
  Icon(MyFlutterApp.humidity_1_),
  Icon(MyFlutterApp.lock_filled),
  Icon(MyFlutterApp.lock_open),
  Icon(MyFlutterApp.moon),
  Icon(MyFlutterApp.night),
  Icon(MyFlutterApp.ok),
  Icon(MyFlutterApp.search),
  Icon(MyFlutterApp.signal),
  Icon(MyFlutterApp.sun),
  Icon(MyFlutterApp.sun_1),
  Icon(MyFlutterApp.temperatire),
  Icon(MyFlutterApp.temperature),
  Icon(MyFlutterApp.user),
  Icon(MyFlutterApp.user_outline),
  Icon(MyFlutterApp.wifi),
  Icon(MyFlutterApp.wifi_1),
];

List<String> deviceImages = ["lamp.svg", "air-conditioner.svg"];
