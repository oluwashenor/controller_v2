import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqlite_api.dart';
import 'models.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initFK() async {
    return await openDatabase(join(await getDatabasesPath(), 'quick_button.db'),
        onCreate: (db, version) async {
      await db.execute('''
  await db.execute('PRAGMA foreign_key = ON'
 ''');
    }, version: 1);
  }

  initDB() async {
    return await openDatabase(join(await getDatabasesPath(), 'quick_button.db'),
        onCreate: (db, version) async {
      await db.execute('''
  CREATE TABLE buttons(
    id INTEGER PRIMARY KEY,pin TEXT,icon TEXT,fontFamily TEXT, fontPackage TEXT, codePoint INTEGER, state INTEGER
  )
 ''');
    }, version: 1);
  }

  // newButton(Button newButton) async {

  //   final db = await database;
  //   List<Map> jsons = await db.rawQuery('SELECT * FROM buttons');
  //    print(jsons.length.toString() + "Data gotten from the Table");
  //    if(jsons.length < 4){
  //       var res = await db.rawInsert('''
  //   INSERT INTO buttons (
  //     pin, icon, codePoint, fontFamily, fontPackage, state
  //   ) VALUES (?, ?, ?, ?, ?, ?) ''', [
  //     newButton.pin,
  //     newButton.icon,
  //     newButton.codePoint,
  //     newButton.fontFamily,
  //     newButton.fontPackage,
  //     newButton.state ? 1 : 0
  //   ]);

  //   print(
  //       "THis is the Inserted value pin ${newButton.pin} , icon ${newButton.icon}, codePoint ${newButton.codePoint} button state ${newButton.state}");
  //   return res;
  //    }
  //    else{
  //      print("you cant create more than 4 Buttons");
  //    }

  // }

  Future<List<Map<String, dynamic>>> querryButtons() async {
    final db = await database;
    return await db.query("buttons");
  }

  Future<int> buttonsQuantity() async {
    final db = await database;
    List<Map> jsons = await db.rawQuery('SELECT * FROM buttons');
    print("There are ${jsons.length} Buttons in the Database");
    return jsons.length;
  }

  // Future<List<Button>> allButtons() async {
  //   final db = await database;
  //   final List<Map<String, dynamic>> maps = await db.query("buttons");
  //   return List.generate(maps.length, (i) {
  //     return Button(
  //         id: maps[i]['id'],
  //         pin: maps[i]['pin'],
  //         icon: maps[i]['icon'],
  //         codePoint: maps[i]['codePoint'],
  //         fontFamily: maps[i]['fontFamily'],
  //         fontPackage: maps[i]['fontPackage'],
  //         state: maps[i]['state'] == 1 ? true : false
  //         //state: false
  //         );
  //   });
  // }

  Future<int> deleteButton(int id) async {
    final db = await database;
    return await db.delete("buttons", where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateButtonState(int state, int id) async {
    var row = {'state': state};

    final db = await database;
    return await db.update("buttons", row, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateButton(Button updateButton, int id) async {
    var row = {'pin': updateButton.pin};
    final db = await database;
    return await db.update("buttons", row, where: 'id = ?', whereArgs: [id]);
  }
}
