import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';
import 'models.dart';

class DatabaseHelper {
  DatabaseHelper._();
  static final DatabaseHelper db = DatabaseHelper._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    return await openDatabase(join(await getDatabasesPath(), 'controller.db'),
        onCreate: (db, version) async {
      await db.execute('PRAGMA foreign_key = ON');
      await db.execute('''

  CREATE TABLE rooms(
    room_id INTEGER PRIMARY KEY,name TEXT,icon TEXT
  )
 ''');

      await db.execute('PRAGMA foreign_key = ON');

      await db.execute('''
  CREATE TABLE buttons(
    id INTEGER PRIMARY KEY,
    pin TEXT,
    type TEXT,
    icon TEXT,
    name TEXT, 
    state INTEGER
  )
 ''');

      await db.execute('''
  CREATE TABLE devices(
    id INTEGER PRIMARY KEY,
    pin TEXT,
    name TEXT,
    icon TEXT,
    room_id INTEGER,
    state INTEGER,
    FOREIGN KEY(room_id) REFERENCES rooms(room_id) ON DELETE CASCADE
  )
 ''');
    }, version: 1);
  }

  newRoom(Room newRoom) async {
    final db = await database;
    var res = await db.rawInsert('''
    INSERT INTO rooms (
      name, icon
    ) VALUES (?, ?) ''', [newRoom.name, newRoom.icon]);
    List<Map> jsons = await db.rawQuery('SELECT * FROM rooms');
    print(jsons.length.toString() + "Data gotten from the Room Table");
    return res;
  }

  Future<List<Map<String, dynamic>>> querryRooms() async {
    final db = await database;
    return await db.query("rooms");
  }

  Future<List<Room>> allRooms() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query("rooms");
    return List.generate(maps.length, (i) {
      return Room(
          id: maps[i]['room_id'], name: maps[i]['name'], icon: maps[i]['icon']);
    });
  }

  Future<int> deleteRoom(int id) async {
    final db = await database;
    return await db.delete("rooms", where: 'room_id = ?', whereArgs: [id]);
  }

  newDevice(Device newDevice) async {
    final db = await database;
    var res = await db.rawInsert('''
    INSERT INTO devices (
      pin, icon,name, room_id,state
    ) VALUES (?, ?, ?, ?, ?) ''', [
      newDevice.pin,
      newDevice.icon,
      newDevice.name,
      newDevice.roomId,
      newDevice.state ? 1 : 0
    ]);
    List<Map> jsons = await db.rawQuery('SELECT * FROM devices');
    print(jsons.length.toString() + "Data gotten from the Table");
    print(
        "Values Inserted pin ${newDevice.pin} , Name ${newDevice.name} Belongs to Room ${newDevice.roomId} with this image ${newDevice.icon}");
    return res;
  }

  Future<List<Map<String, dynamic>>> querryDevices() async {
    final db = await database;
    return await db.query("buttons");
  }

  // buttonsQuantity() async {
  //   final db = await database;
  //   List<Map> jsons = await db.rawQuery('SELECT * FROM buttons');
  //   print("There are ${jsons.length} Buttons in the Database");
  //   return jsons.length;
  // }

  roomDevicesQuantity(int roomId) async {
    final db = await database;
    List<Map> jsons =
        await db.query("devices", where: 'room_id  = ?', whereArgs: [roomId]);
    //print("There are ${jsons.length} Buttons in the Database");
    return jsons.length;
  }

  Future<int> deleteDevice(int id) async {
    final db = await database;
    return await db.delete("devices", where: 'id = ?', whereArgs: [id]);
  }

  Future<List<Device>> allRoomDevices(int roomId) async {
    final db = await database;
    final List<Map<String, dynamic>> maps =
        await db.query("devices", where: 'room_id = ?', whereArgs: [roomId]);
    return List.generate(maps.length, (i) {
      return Device(
          id: maps[i]['id'],
          pin: maps[i]['pin'],
          icon: maps[i]['icon'],
          name: maps[i]['name'],
          roomId: maps[i]['roomID'],
          state: maps[i]['state'] == 1 ? true : false);
    });
  }

  newButton(Button newButton) async {
    final db = await database;
    // List<Map> jsons = await db.rawQuery('SELECT * FROM buttons');
    //  print(jsons.length.toString() + "Data gotten from the Table");
    //if(jsons.length == 0 || jsons.length < 4){
    var res = await db.rawInsert('''
    INSERT INTO buttons (
      pin, icon, name, state, type
    ) VALUES (?, ?, ?, ?, ?) ''', [
      newButton.pin,
      newButton.icon,
      newButton.name,
      newButton.state ? 1 : 0,
      newButton.type,
    ]);
    print(
        "THis is the Inserted value pin ${newButton.pin} , icon ${newButton.icon}, name ${newButton.name} button state ${newButton.state} with type ${newButton.type}");
    return res;
  }

  Future<List<Map<String, dynamic>>> querryButtons() async {
    final db = await database;
    return await db.query("buttons");
  }

  Future<int> buttonsQuantity() async {
    final db = await database;
    List<Map> jsons = await db.rawQuery('SELECT * FROM buttons');
    print("There are ${jsons.length} Buttons in the Database");
    return jsons.length;
  }

  Future<List<Button>> allButtons() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query("buttons");
    return List.generate(maps.length, (i) {
      return Button(
          id: maps[i]['id'],
          type: maps[i]['type'],
          pin: maps[i]['pin'],
          icon: maps[i]['icon'],
          name: maps[i]['name'],
          state: maps[i]['state'] == 1 ? true : false);
    });
  }

  Future<int> deleteButton(int id) async {
    final db = await database;
    return await db.delete("buttons", where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateButtonState(int state, int id) async {
    var row = {'state': state};

    final db = await database;
    return await db.update("buttons", row, where: 'id = ?', whereArgs: [id]);
  }

  Future<void> updateButton(Button updateButton, int id) async {
    var row = {'pin': updateButton.pin};
    final db = await database;
    return await db.update("buttons", row, where: 'id = ?', whereArgs: [id]);
  }
}
