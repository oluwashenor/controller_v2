import 'package:shared_preferences/shared_preferences.dart';

profile() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool('profile', true);
}

gender(bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool('male', value);
}

savePassword(String password) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("password_key", password);
}

Future<String> loadPassword() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final password = prefs.getString("password_key");
  print("THis is the passowrd $password");
  return password;
}

saveIP(String ip) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("ip_address_key", ip);
}

Future<String> loadIP() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final myIp = prefs.getString("ip_address_key");
  return myIp;
}

Future createProfile(String name) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("profile_key", name);
}

Future<String> readProfile() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final myString = prefs.getString("profile_key") ?? null;
  return myString;
}

Future<bool> readTheme() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final currentTheme = prefs.getBool("lightTheme");
  if (currentTheme == null) {
    print("I know you are not working now ");
    setTheme(true);
    return true;
  } else {
    return currentTheme;
  }
}

setTheme(bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("lightTheme", value);
}
