import 'package:controller_v2/chat.dart';
import 'package:controller_v2/global.dart';
import 'package:controller_v2/my_flutter_app_icons.dart';
import 'package:controller_v2/profile.dart';
import 'package:controller_v2/providers/theme.dart';
import 'package:controller_v2/settings.dart';
import 'package:controller_v2/utils/DatabaseHelper.dart';
import 'package:controller_v2/utils/data.dart';
import 'package:controller_v2/utils/models.dart';
import 'package:controller_v2/utils/sharedpref.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'apptheme.dart';
import 'lockscreen.dart';

class MyDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class RoomButtomSheet extends StatefulWidget {
  final Function() notifyParent;

  RoomButtomSheet({Key key, @required this.notifyParent}) : super(key: key);
  @override
  _RoomButtomSheetState createState() => _RoomButtomSheetState();
}

class _RoomButtomSheetState extends State<RoomButtomSheet> {
  final TextEditingController namecontroller = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height / 2.3 +
            MediaQuery.of(context).viewInsets.bottom,
        padding: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
          ),
          //  border: Border.all(color: Colors.grey, width: 2),
          color: Theme.of(context).appBarTheme.color,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              color: Colors.transparent,
              padding: EdgeInsets.only(top: 30, left: 20),
              alignment: Alignment.bottomLeft,
              child: Text(
                "Create new room",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ),
            Column(children: <Widget>[
              Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.only(top: 20, right: 20, left: 20),
                  child: MyTextField(c: namecontroller, name: "Name"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                        icon: Icon(Icons.photo),
                        onPressed: () {
                          buildImageDialog(context);
                        }),
                    // IconButton(icon: Icon(Icons.add_a_photo), onPressed: () {})
                  ],
                ),
              ),
              SizedBox(
                width: 300,
                child: RaisedButton(
                    elevation: 1,
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        if (selectedRoomImage != null) {
                          var newDBRoom = Room(
                            icon: selectedRoomImage,
                            name: namecontroller.text,
                          );
                          DatabaseHelper.db.newRoom(newDBRoom);
                          Navigator.pop(context);
                          namecontroller.clear();
                          selectedRoomImage = null;
                          widget.notifyParent();
                          Fluttertoast.showToast(
                              backgroundColor: Colors.black,
                              textColor: Colors.white,
                              msg: "Room Created Successfully");
                        } else {
                          buildImageDialog(context);
                          print("Please Select an Image");
                        }
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 10),
                      child: Text(
                        "Create Room",
                        style: TextStyle(color: Colors.white),
                      ),
                    )),
              ),
            ])
          ],
        ));
  }

  buildImageDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
              titlePadding: EdgeInsets.all(0),
              contentPadding: EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              children: <Widget>[
                Container(
                  height: 800,
                  width: 600,
                  child: GridView.builder(
                    scrollDirection: Axis.vertical,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3),
                    itemCount: assetimages.length,
                    itemBuilder: (BuildContext context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            print("image selected is ${assetimages[index]}");
                            selectedRoomImage = "${assetimages[index]}";
                            print(selectedRoomImage);
                            Navigator.pop(context);
                          },
                          child: SvgPicture.asset(
                              "assets/svgicons/${assetimages[index]}",
                              semanticsLabel: "Logo"),
                        ),
                      );
                    },
                  ),
                )
              ]);
        });
  }
}

class ButtonButtomSheet extends StatefulWidget {
  final Function() notifyParent;

  ButtonButtomSheet({Key key, @required this.notifyParent}) : super(key: key);

  @override
  _ButtonButtomSheetState createState() => _ButtonButtomSheetState();
}

class _ButtonButtomSheetState extends State<ButtonButtomSheet> {
  final TextEditingController pincontroller = new TextEditingController();
  final TextEditingController namecontroller = new TextEditingController();

  final _formKey = GlobalKey<FormState>();
  String type;

  @override
  Widget build(BuildContext context) {
    // return Container(
    //   decoration: BoxDecoration(
    //     color: Theme.of(context).appBarTheme.color,
    //   ),
    //   height: MediaQuery.of(context).size.height / 2 +
    //       MediaQuery.of(context).viewInsets.bottom,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     mainAxisSize: MainAxisSize.min,
    //     children: <Widget>[
    //       Container(
    //         color: Colors.transparent,
    //         padding: EdgeInsets.only(top: 20, left: 20),
    //         alignment: Alignment.bottomLeft,
    //         child: Text(
    //           "Create new button",
    //           style: Theme.of(context).textTheme.subtitle1,
    //         ),
    //       ),
    //       Column(
    //         children: <Widget>[
    //           Padding(
    //             padding: const EdgeInsets.all(20.0),
    //             child: Form(
    //                 key: _formKey,
    //                 autovalidateMode: AutovalidateMode.disabled,
    //                 child: Column(children: <Widget>[
    //                   Row(
    //                     children: [
    //                       Expanded(
    //                         child: MyTextField(c: namecontroller, name: "Name"),
    //                       ),
    //                     ],
    //                   ),
    //                   Row(
    //                     children: [
    //                       Expanded(
    //                         child: MyTextField(c: pincontroller, name: "Pin"),
    //                       ),
    //                     ],
    //                   ),

    //                   Row(
    //                     mainAxisAlignment: MainAxisAlignment.start,
    //                     children: [
    //                       Expanded(
    //                         child: RaisedButton(
    //                             elevation: 0,
    //                             shape: RoundedRectangleBorder(
    //                                 side: BorderSide(
    //                                   color: Theme.of(context).accentColor,
    //                                 ),
    //                                 borderRadius: BorderRadius.circular(5)),
    //                             color: Theme.of(context).appBarTheme.color,
    //                             onPressed: () {
    //                               buildIconDialog(context);
    //                             },
    //                             child: Padding(
    //                               padding: const EdgeInsets.symmetric(
    //                                   horizontal: 10.0, vertical: 20),
    //                               child: Row(
    //                                 mainAxisAlignment:
    //                                     selectedButtonIcon == null
    //                                         ? MainAxisAlignment.center
    //                                         : MainAxisAlignment.spaceEvenly,
    //                                 children: [
    //                                   Text(
    //                                     selectedButtonIcon != null
    //                                         ? "Change"
    //                                         : "Select Icon",
    //                                     style: TextStyle(color: Colors.black),
    //                                   ),
    //                                   AnimatedSwitcher(
    //                                       duration: Duration(milliseconds: 500),
    //                                       child: selectedButtonIcon != null
    //                                           ? Icon(IconData(
    //                                               int.parse(selectedButtonIcon),
    //                                               fontFamily: "MyFlutterApp"))
    //                                           : Container()),
    //                                 ],
    //                               ),
    //                             )),
    //                       ),
    //                       SizedBox(width: 20),
    //                       Expanded(
    //                         child: RaisedButton(
    //                             elevation: 1,
    //                             color: Theme.of(context).accentColor,
    //                             shape: RoundedRectangleBorder(
    //                                 borderRadius: BorderRadius.circular(5)),
    //                             onPressed: () {
    //                               if (_formKey.currentState.validate()) {
    //                                 if (selectedButtonIcon != null) {
    //                                   var newDBButton = Button(
    //                                       icon: selectedButtonIcon,
    //                                       pin: pincontroller.text,
    //                                       name: namecontroller.text,
    //                                       state: false);
    //                                   DatabaseHelper.db.newButton(newDBButton);
    //                                   widget.notifyParent();
    //                                   selectedButtonIcon = null;
    //                                   Navigator.pop(context);
    //                                   Fluttertoast.showToast(
    //                                       backgroundColor: Colors.black,
    //                                       textColor: Colors.white,
    //                                       msg: "Button Created Successfully");
    //                                 } else {
    //                                   buildIconDialog(context);
    //                                 }
    //                               }
    //                             },
    //                             child: Padding(
    //                               padding: const EdgeInsets.symmetric(
    //                                   vertical: 20.0, horizontal: 10),
    //                               child: Text(
    //                                 "Create Button",
    //                                 style: TextStyle(color: Colors.white),
    //                               ),
    //                             )),
    //                       ),
    //                     ],
    //                   ),
    //                   //SizedBox(height: 20),
    //                 ])),
    //           )
    //         ],
    //       )
    //     ],
    //   ),
    // );

    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).appBarTheme.color,
      ),
      height: MediaQuery.of(context).size.height / 1.7 +
          MediaQuery.of(context).viewInsets.bottom,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            color: Colors.transparent,
            padding: EdgeInsets.only(top: 20, left: 20),
            alignment: Alignment.bottomLeft,
            child: Text(
              "Create button",
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.disabled,
                    child: Column(children: <Widget>[
                      Row(
                        children: [
                          Expanded(
                            child: MyTextField(c: namecontroller, name: "Name"),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: MyTextField(
                                c: pincontroller, name: "Action/Pin"),
                          ),
                        ],
                      ),

                      Row(
                        children: [
                          Expanded(
                            child: Container(
                                padding: EdgeInsets.symmetric(vertical: 10),
                                child: DropdownButtonFormField<String>(
                                  iconEnabledColor:
                                      Theme.of(context).accentColor,
                                  decoration: InputDecoration(
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                            color:
                                                Colors.black.withOpacity(0.5))),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).accentColor)),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                            color:
                                                Theme.of(context).accentColor)),
                                  ),
                                  validator: (value) {
                                    var response;
                                    if (value == null) {
                                      response = "Please Select an option";
                                    } else {
                                      response = null;
                                    }
                                    return response;
                                  },
                                  hint: Text("Button Type",
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle2),
                                  style: Theme.of(context).textTheme.subtitle2,
                                  items: <String>["button", "custom"]
                                      .map((String e) {
                                    return DropdownMenuItem<String>(
                                      value: e,
                                      child: Text(e),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    type = value;
                                  },
                                )),
                          )
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: RaisedButton(
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                      color: Theme.of(context).accentColor,
                                    ),
                                    borderRadius: BorderRadius.circular(5)),
                                color: Theme.of(context).appBarTheme.color,
                                onPressed: () {
                                  buildIconDialog(context);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                        selectedButtonIcon == null
                                            ? MainAxisAlignment.center
                                            : MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                          selectedButtonIcon != null
                                              ? "Change"
                                              : "Select Icon",
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle2),
                                      AnimatedSwitcher(
                                          duration: Duration(milliseconds: 500),
                                          child: selectedButtonIcon != null
                                              ? Icon(IconData(
                                                  int.parse(selectedButtonIcon),
                                                  fontFamily: "MyFlutterApp"))
                                              : Container()),
                                    ],
                                  ),
                                )),
                          ),
                          SizedBox(width: 20),
                          Expanded(
                            child: RaisedButton(
                                elevation: 1,
                                color: Theme.of(context).accentColor,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    if (selectedButtonIcon != null) {
                                      var newDBButton = Button(
                                          icon: selectedButtonIcon,
                                          pin: pincontroller.text,
                                          name: namecontroller.text,
                                          type: type,
                                          state: false);
                                      DatabaseHelper.db.newButton(newDBButton);
                                      widget.notifyParent();
                                      selectedButtonIcon = null;
                                      Navigator.pop(context);
                                      Fluttertoast.showToast(
                                          backgroundColor: Colors.black,
                                          textColor: Colors.white,
                                          msg: "Button Created Successfully");
                                    } else {
                                      buildIconDialog(context);
                                    }
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 20.0, horizontal: 10),
                                  child: Text(
                                    "Create Button",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                )),
                          ),
                        ],
                      ),
                      //SizedBox(height: 20),
                    ])),
              )
            ],
          )
        ],
      ),
    );
  }

  buildIconDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
              titlePadding: EdgeInsets.all(0),
              contentPadding: EdgeInsets.all(10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              children: <Widget>[
                Container(
                  height: 500,
                  width: 500,
                  child: GridView.builder(
                    scrollDirection: Axis.vertical,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        childAspectRatio: 1.0, crossAxisCount: 4),
                    itemCount: myIcons.length,
                    itemBuilder: (BuildContext context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            selectedButtonIcon =
                                "${myIcons[index].icon.codePoint}";
                            print(myIcons[index]);
                            print(myIcons[index].icon);
                            print(myIcons[index].icon.codePoint);
                            print(myIcons[index].icon.fontFamily);
                            print(myIcons[index].icon.fontPackage);
                            Navigator.pop(context);
                          },
                          child: myIcons[index],
                        ),
                      );
                    },
                  ),
                )
              ]);
        });
  }
}

class MyTextField extends StatelessWidget {
  final TextEditingController c;
  final String name;

  const MyTextField({Key key, this.c, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        child: TextFormField(
          validator: (value) {
            if (value.isEmpty) {
              return "This field is required";
            } else {
              return null;
            }
          },
          controller: c,
          decoration: InputDecoration(
              labelText: name,
              errorStyle: TextStyle(fontSize: 10),
              labelStyle: Theme.of(context).textTheme.subtitle2,
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Theme.of(context).accentColor)),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: BorderSide(color: Colors.black))),
        ));
  }
}

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeChanger>(context);
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Column(children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text(fullname, style: TextStyle(color: Colors.white)),
          accountEmail: Text(email, style: TextStyle(color: Colors.white)),
          otherAccountsPictures: <Widget>[
            Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ViewProfile(),
                    ));
                  },
                  child: Icon(MyFlutterApp.user_outline, color: Colors.white)),
            ),
            Align(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () async {
                    var theme = await readTheme();
                    if (theme == true) {
                      themeChanger.setTheme(Apptheme.darktheme);
                      setTheme(false);
                    } else {
                      themeChanger.setTheme(Apptheme.lighttheme);
                      setTheme(true);
                    }
                  },
                  child: Icon(MyFlutterApp.moon, color: Colors.white)),
            ),
          ],
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                  'assets/images/0b63383c069fb82f1e88c5720182f1a8.jpg'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => Chatroom(),
            ));
          },
          child: ListTile(
            dense: true,
            leading: Icon(Icons.chat_bubble_sharp,
                color: Theme.of(context).iconTheme.color),
            title: Text(
              "Chatroom",
              style: Theme.of(context).primaryTextTheme.subtitle1,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => LockScreen(),
            ));
          },
          child: ListTile(
            dense: true,
            leading: Icon(Icons.lock, color: Theme.of(context).iconTheme.color),
            title: Text(
              "Lock App",
              style: Theme.of(context).primaryTextTheme.subtitle1,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => Settings(),
            ));
          },
          child: ListTile(
            dense: true,
            leading:
                Icon(Icons.settings, color: Theme.of(context).iconTheme.color),
            title: Text(
              "Settings",
              style: Theme.of(context).primaryTextTheme.subtitle1,
            ),
          ),
        ),
        GestureDetector(
          child: ListTile(
            dense: true,
            leading:
                Icon(Icons.settings, color: Theme.of(context).iconTheme.color),
            title: Text(
              "Admin",
              style: Theme.of(context).primaryTextTheme.subtitle1,
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            SystemNavigator.pop();
          },
          child: ListTile(
            dense: true,
            leading:
                Icon(Icons.close, color: Theme.of(context).iconTheme.color),
            title: Text(
              "Quit App",
              style: Theme.of(context).primaryTextTheme.subtitle1,
            ),
          ),
        )
      ]),
    );
  }
}

class IPDialog extends StatefulWidget {
  final Function() notifyParent;

  const IPDialog({Key key, this.notifyParent}) : super(key: key);
  @override
  _IPDialogState createState() => _IPDialogState();
}

class _IPDialogState extends State<IPDialog> {
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  final TextEditingController ipcontroller = new TextEditingController();

  updateIP(String value) {
    setState(() {
      address = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        titlePadding: EdgeInsets.all(0),
        contentPadding: EdgeInsets.all(10),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15))),
        children: <Widget>[
          Form(
            key: _formkey,
            child: Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(
                    new Radius.circular(50),
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0, bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Set IP Address",
                            style: TextStyle(
                                fontSize: 14, fontWeight: FontWeight.w600),
                          )
                        ],
                      ),
                    ),
                    TextFormField(
                      style: Theme.of(context).textTheme.headline6,
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return ("This field cannont be empty");
                        }
                        {
                          return null;
                        }
                      },
                      onSaved: (value) {
                        //   name = value;
                      },
                      controller: ipcontroller,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: BorderSide(
                                  color: Theme.of(context).accentColor)),
                          hintStyle: TextStyle(color: Colors.grey),
                          hintText: address),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 35.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new MaterialButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(15)),
                              padding: EdgeInsets.all(10),
                              hoverElevation: 0,
                              minWidth: 100,
                              height: 50,
                              color: Theme.of(context).accentColor,
                              onPressed: () {
                                saveIP(ipcontroller.text);
                                updateIP(ipcontroller.text);
                                ipcontroller.clear();
                                Navigator.pop(context);
                                Fluttertoast.showToast(
                                    backgroundColor: Colors.black,
                                    textColor: Colors.white,
                                    msg: "IP Address Saved");
                              },
                              child: Text(
                                "Save",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ]),
                    )
                  ],
                )),
          ),
        ]);
  }
}

class DeleteRoomDialog extends StatefulWidget {
  final int index;
  final Function() notifyParent;

  DeleteRoomDialog({Key key, this.index, this.notifyParent}) : super(key: key);
  @override
  _DeleteRoomDialogState createState() => _DeleteRoomDialogState();
}

class _DeleteRoomDialogState extends State<DeleteRoomDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Delete Room"),
      content: Text("Are you sure you want to delete this room ?"),
      actions: [
        FlatButton(
            onPressed: () {
              //Navigator.pop(context);
              print(widget.index);
            },
            child: Text("Cancel")),
        FlatButton(
            onPressed: () {
              DatabaseHelper.db.deleteRoom(roomslist[widget.index].id);
              roomslist.removeAt(widget.index);
              widget.notifyParent();
              Navigator.pop(context);
            },
            child: Text(
              "Delete",
              style: TextStyle(color: Colors.redAccent),
            ))
      ],
    );
  }
}

// class ConfigureDialog extends StatelessWidget {
//   final Button button;
//   final Function() notifyParent;

//   ConfigureDialog({Key key, this.button, this.notifyParent}) : super(key: key);
//   final TextEditingController pincontroller = new TextEditingController();
//   final TextEditingController namecontroller = new TextEditingController();
//   @override
//   Widget build(BuildContext context) {
//     return SimpleDialog(
//         backgroundColor: Theme.of(context).appBarTheme.color,
//         titlePadding: EdgeInsets.all(0),
//         contentPadding: EdgeInsets.all(10),
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.all(Radius.circular(15))),
//         children: <Widget>[
//           Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
//             Container(
//               color: Colors.transparent,
//               padding: EdgeInsets.only(top: 10, left: 20),
//               alignment: Alignment.center,
//               child: Text(
//                 "Configure ${button.name}",
//                 style: Theme.of(context).textTheme.subtitle1,
//               ),
//             ),
//             Form(
//                 child: Column(children: <Widget>[
//               Row(
//                 children: [
//                   Expanded(
//                     child: MyTextField(c: namecontroller, name: "Name"),
//                   ),
//                 ],
//               ),
//               Row(
//                 children: [
//                   Expanded(
//                     child: MyTextField(c: pincontroller, name: "${button.pin}"),
//                   ),
//                 ],
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   Expanded(
//                     child: RaisedButton(
//                         elevation: 0,
//                         shape: RoundedRectangleBorder(
//                             side: BorderSide(
//                               color: Theme.of(context).accentColor,
//                             ),
//                             borderRadius: BorderRadius.circular(5)),
//                         color: Theme.of(context).appBarTheme.color,
//                         onPressed: () {
//                           //  _pickIcon();
//                         },
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(
//                               horizontal: 5.0, vertical: 18),
//                           child: Row(
//                             children: [
//                               Text(
//                                 // _icon != null
//                                 //     ? "Change Icon"
//                                 "Select Icon",
//                                 style: TextStyle(color: Colors.black),
//                               ),
//                               // AnimatedSwitcher(
//                               //     duration: Duration(milliseconds: 500),
//                               //     child: _icon != null
//                               //         ? Icon(iconData)
//                               //         : Container()),
//                             ],
//                           ),
//                         )),
//                   ),
//                 ],
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   Expanded(
//                     child: RaisedButton(
//                         elevation: 1,
//                         color: Theme.of(context).accentColor,
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(5)),
//                         onPressed: () {
//                           DatabaseHelper.db.deleteButton(button.id);
//                           notifyParent();
//                           //buttonslist.removeAt(index);
//                           // setState(() {});
//                           //updateUiforButtons();

//                           Navigator.pop(context);
//                         },
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(
//                               vertical: 20.0, horizontal: 10),
//                           child: Text(
//                             "Delete Button",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         )),
//                   ),
//                 ],
//               ),
//               SizedBox(height: 10),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: [
//                   Expanded(
//                     child: RaisedButton(
//                         elevation: 1,
//                         color: Theme.of(context).accentColor,
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(5)),
//                         onPressed: () {
//                           print("New pin is " + pincontroller.text);
//                           var updateDBButton = Button(
//                             pin: pincontroller.text,
//                           );
//                           DatabaseHelper.db
//                               .updateButton(updateDBButton, button.id);
//                           //updateUiforButtons();
//                           Navigator.pop(context);
//                         },
//                         child: Padding(
//                           padding: const EdgeInsets.symmetric(
//                               vertical: 20.0, horizontal: 10),
//                           child: Text(
//                             "Update Button",
//                             style: TextStyle(color: Colors.white),
//                           ),
//                         )),
//                   ),
//                 ],
//               ),
//               SizedBox(height: 10),
//             ])),
//           ])
//         ]);
//   }
// }
